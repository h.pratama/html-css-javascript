// // let varb = 1;

// // console.log(varb);

// // varb = 2
// // console.log(varb, '<< value sudah diganti');

// // const varbConst = 1
// // console.log(varbConst, '<< value pertama');

// // varbConst = 2
// // console.log(varbConst, '<< value sudah diganti');

// const str = 'Halo'
// const int = 10
// const bool = true | false 

// const obj = {
//     name: 'Budy',
//     age: 16,
//     address: {
//         name: 'Jakarta',
//         district: 'Jakarta Timur',
//         no: 2
//     }
// }

// // obj['address']['name'] = 'budi@mail.com'
// // obj.address.name = 'budi@mail.com'

// // console.log(obj.name);
// // console.log(obj['age']);

// // const { name, age } = obj;

// // console.log(name, age);
// // console.log(obj);

// // console.log(obj.address.no);
// // console.log(obj['address']['no']);

// // const { address: { name, district, no } } = obj;

// // console.log(name, '<< ');

// // const arr = [1, '3', true, { name: 'Budi' } ]

// // arr[3] = 2

// // console.log(arr);

// // const [ idx0, angka3, boolean, objUser ] = arr;

// // console.log(objUser);

// // const users =  [
// //     {
// //         name: 'Budi',
// //         email: 'budi@mail.com'
// //     },
// //     {
// //         name: 'John',
// //         email: 'john@mail.com'
// //     },
// //     {
// //         name: 'John',
// //         email: 'john@mail.com'
// //     },
// // ]

// // console.log(users.length);
// // console.log(users[0].name.length);

// // function printHello() {
    
// //     return 'Hallo user'
// // }

// // console.log(printHello());
// // function printHello(param) {
// //     console.log('Hallo ' + param);
// // }

// // printHello('Budi')

// // for (let idx = 1; idx <=10; idx++) {
// //     console.log(idx);

// //     for (let J = 0; J < 10; J++) {
// //         console.log(J, '<< j');
// //     }
// // }

// // const arr = [1,2,3,4,5,6,7,8];

// // const users =  [
// //     {
// //         name: 'Budi',
// //         email: 'budi@mail.com'
// //     },
// //     {
// //         name: 'John',
// //         email: 'john@mail.com'
// //     },
// //     {
// //         name: 'John',
// //         email: 'john@mail.com'
// //     },
// // ]

// // for (let index = 0; index < users.length; index++) {
// //     console.log(users[index].name);
// // }

// // for (const key in users) {
// //     console.log(key, '<< index arr');
// // }
// // for (const key in obj) {
// //     console.log(obj[key]);
// // }

// // console.log(1 + 1);
// // console.log(1 - 1);
// // console.log(4 / 2);
// // console.log(4 * 2);
// // console.log(10 % 2);

// // console.log(obj);

// // const obj2 = {
// //     email: 'budi@mail.com',
// //     lastName: 'Doremi'
// // }

// // const mergeObj = { ...obj, ...obj2 } // spread operator

// // console.log(mergeObj);

// // const arr1 = [1,2,3];
// // const arr2 = [4,5,6];

// // const mergeArr = [ ...arr1, obj.name ]

// // console.log(mergeArr);

// // for (const key in obj2) {
//     // console.log(obj2[key]);
// //     arr1.push(obj2[key])
// // }

// // console.log(arr1);

// // function hallo() {
// //     return 'Hallo'
// // }

// // console.log(hallo());

// // const arrFunc = (name) => {
// //     return 'Hallo ini dari arrFunc ' + name 
// // }

// // console.log(arrFunc('Budi'));

// // function nameFunc() {
// //     setTimeout(() => {
// //         console.log('hallo');
// //     }, 3000)
// // }

// // nameFunc()

// // console.log('Hallo 2');

// const promise = new Promise( (resolve, reject) => {
//     const isTrue = false;

//     if (isTrue) {
//         setTimeout(() => {
//             resolve('Ini sukses')
//         }, 3000)
//     } else {
//         reject('Error action')
//     }

// })


// // syncronous
// // function callPromise() {

// //     promise
// //     .then(success => {
// //         console.log(success, '<< success');
// //     })
// //     .catch(err => {
// //         console.log(err, '<<, err');
// //     })

// //     console.log('Hallo');
// // }

// // async
// async function callPromise() {
//     try {
//         const getOpt = await promise
    
//         console.log(getOpt, '<< getOpt');
    
//         console.log('Hallo');
//     } catch (error) {
//         console.log(error, '<< ini error');
//     }

// }

// callPromise()

// const obj = {
//     property: ''
// }

class Car {
    name = 'Avanza';
    type = 'Toyota';
    km = 100000;

    constructor(nameParam, typeParam, kmParam) {
        if (nameParam) this.name = nameParam
        if (typeParam) this.type = typeParam
        if (kmParam) this.km = kmParam
    }

    printSomething(user) {
        // return 'Hallo user ' + user;
        
        return `Hallo user ${user} mobil anda ${this.name} bertipe ${this.type} dengan jarak tempuh ${this.km} km dan mempunyai`;
    }

    printAnother() {

         const getVal = this.printSomething('Budi')
         return getVal
    }
}

// const newCar = new Car('L300', 'Mitsubishi', 100)

// console.log(newCar);

// const defaultCar = new Car()

// console.log(defaultCar);

// console.log(newCar.printSomething('John'));
// console.log(newCar.printAnother());

class Toyota extends Car {
    tire = ''

    constructor(name, km, tireParam) {
        super(name, 'Toyota', km)

        this.tire = tireParam
    }

    detailCar(userName) {
        return this.printSomething(userName) + ` roda ${this.tire}`
    }
}

const newToyota = new Toyota('Veloz', 1000, 4);

console.log(newToyota.detailCar('Budi'));
